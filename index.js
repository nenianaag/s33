// 3. Create a fetch request using GET method that will retrieve all the to do list items from JSON Placeholder APIs.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => console.log(data))

// 4. Using the data retrieved, create an array using the MAP method to return just the title of every item and print the result in the console.
// 
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then(data => {
	let toDoArray = data.map((elem) => {
		return elem.title
	})
	console.log(toDoArray)
})
// 5. Create a fetch request using GET method that will retrieve a single to do list from JSON Placeholder API.
// 6. Using data retrieved, print a message in the console that provide the title and status of the to do list item.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => {
	console.log(data)
	console.log(`The item "${data.title}" on the list has a status of ${data.completed}`)
})


// 7. Create a fetch request using POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers:  {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


// 8. Create a fetch request using PUT method that will update a to do list item using JSON Placeholder API.

// 9. Update a to do list item by changing the data structure to contain the following properties:
/*
	a. Title
	b. Decription
	c. Status
	d. Date Completed
	e. User ID
*/
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers:  {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		status: "Pending",
		userId: 1,
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure"
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


// 10. Create a fetch request using the PATCH method that will update a to do list using JSON Placeholder API.
// 11. Update a to do list by changing the status to complete and add a date when the status was changed
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers:  {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "delectus aut autem",
		completed: false,
		dateCompleted: "07/09/21",
		status: "Complete"
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 12. Create a fetch request using DELETE method that will delete an item using JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'DELETE'
})

// 13. Create a request via POSTMAN to retrieve all the to do list items.
/*
	a. GET HTTP method
	b. https://jsonplaceholder.typicode.com/todos URI endpoint
	c. Save this request as get all to do list items
*/

// 14. Create a request via POSTMAN to retrieve an individual to do list item.
/*
	a. GET HTTP method
	b. https://jsonplaceholder.typicode.com/todos URI endpoint
	c. Save this request as get all to do list items
*/

// 15. Create a request via POSTMAN to create a to do list item.
/*
	a. POST HTTP method
	b. https://jsonplaceholder.typicode.com/todos URI endpoint
	c. Save this request as get all to do list items
*/

// 16. Create a request via POSTMAN to update to do list item.
/*
	a. PUT HTTP method
	b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	c. Save this request as get all to do list items
	d. Update the to do list item to mirror the data structure used in the PUT fetch request
*/

// 17. Create a request via POSTMAN to update a to do list item.
/*
	a. PATCH HTTP method
	b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	c. Save this request as get all to do list items
	d. Update the to do list item to mirror the data structure used in the PATCH fetch request
*/

// 18. Create a request via POSTMAN to delete a to do list item.
/*
	a. DELETE HTTP method
	b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
	c. Save this request as delete to do list items
*/

